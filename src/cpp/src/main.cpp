
#include <iostream>
#include <pthread.h>
#include <string>
#include "diffrobot.h"
//static ofstream myfile;
int main(int argc, char *argv[])
{
	if ((argc < 2) || (argc > 3)) {     // Test for correct number of arguments
    cerr << "Usage: " << argv[0]
         << " <IP address> <Port>" << endl;
    exit(1);
  }

	string ip_addr = argv[1];
	string port = argv[2];



	DiffRobot differential_robot(ip_addr, port);

	pthread_t * thptr;

	thptr = differential_robot.get_driver_reading_thread();

	pthread_join(*thptr, NULL);
	//std::cout<<"Hello world.";
  return 0;
}
