

#include <string>
#include "interruptdata.h"

using namespace std;

InterruptData::InterruptData(){
  this->value = 0;
  this->sec = 0;
  this->usec = 0;
}

InterruptData::InterruptData(int value, int sec, int usec){
  this->value = value;
  this->sec = sec;
  this->usec = usec;
}

InterruptData::InterruptData(const InterruptData &a){
  this->value = a.value;
  this->sec = a.sec;
  this->usec = a.usec;
}

double InterruptData::operator- (InterruptData p){										// usec iin limit 1 say. 1 say bolchihood sec negeer nemegdene
  double result, temp;
  if(this->sec==p.sec){
    result=0;
    result=this->usec-p.usec;
    result=result/1000000;
  }
  else{
    temp=this->sec-p.sec-1;
    result=(1000000+this->usec)-p.sec+temp*1000000;
    result=result/1000000;
  }
  return result;
}

string InterruptData::convert_string(){
  string st;
  st = to_string(value)+" "+to_string(sec) + " " + to_string(usec) + "\n";
  return st;
}
