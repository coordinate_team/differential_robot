
#include <math.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <pthread.h>
#include <linux/input.h>
#include <cmath>

#include "point.h"
#include "cubebezier.h"
#include "interruptdata.h"
#include "diffrobot.h"
#include "inetclientstream.hpp"
#include "practical_socket.h"

#define pwm_path "/sys/devices/ocp.3/pwm_test_P8_13.16"
#define pi 3.1416


static pthread_mutex_t writelock = PTHREAD_MUTEX_INITIALIZER;

using namespace std;

DiffRobot::DiffRobot(){
  L = 212.5;
  M = 165;
  delta = 0.4;
  theta = 0;
  velA_wheel = 0;
  velB_wheel = 0;
  index = 0;

  gettimeofday(&start_time, NULL);  // stamping start time

  // read bezier curves
  get_Bezier_trajectory();

  // start thread
  //strcpy(rotary_driver, "/dev/input/event1");
  //pthread_create (&driver_reading_thread, NULL, &DiffRobot::driverReading, (void *) this);

  // TCP
  string host = "::1";
  string port = "1235";
  string answer;
  //double test_double = 12.0;
  answer.resize(32);

  //string st = to_string(test_double);
  //st = "hello from diff robot" + st;
  //cout<<st<<endl;
  try {
     tcp_socket = new libsocket::inet_stream(host,port,LIBSOCKET_IPv4);
     //*tcp_socket >> answer;
     //cout << answer;
     //*tcp_socket<<"dfdfdfdfd\n";
  }
  catch (const libsocket::socket_exception& exc)
  {
    cerr << exc.mesg;
  }
  send_bezier_curves();
}

DiffRobot::DiffRobot(string ip_addr, string port){
  velA_wheel = 0;
  velB_wheel = 0;
  robot_config("robot_config.txt");

  //cout<<"pos y: "<<position.getY()<<endl;

  gettimeofday(&start_time, NULL);  // stamping start time

  // read bezier curves
  get_Bezier_trajectory();

  // start thread
  strcpy(rotary_driver, "/dev/input/event1");
  pthread_create (&driver_reading_thread, NULL, &DiffRobot::driverReading, (void *) this);

  // TCP
  //string answer;
  //double test_double = 12.0;
  //answer.resize(32);

  //string st = to_string(test_double);
  //st = "hello from diff robot" + st;
  //cout<<st<<endl;
  /*
  try {
     //tcp_socket = new libsocket::inet_stream(ip_addr, port, LIBSOCKET_IPv4);
  }
  catch (const libsocket::socket_exception& exc)
  {
    cerr << exc.mesg;
  }*/
  try {
    const char *temp_addr = ip_addr.c_str();
    unsigned short temp_port = atoi(port.c_str());
    tcp_socket_ps = new TCPSocket(temp_addr, temp_port);
  }
  catch(SocketException &e) {
    cerr << e.what() << endl;
    exit(1);
  }
  //send_bezier_curves();
  //send_posAB();
  send_bezier_curves_ps();
  //send_posAB_ps();
  //send_posAB();
  //send_posAB();
  interrupt_data_file_log.open("log/interrupt_datas.txt");
  if (interrupt_data_file_log.is_open())
  {
    cout<<"interrupt data_log file opened"<<endl;
  }
  else {
    cout<<"can't open interrupt data log file!"<<endl;
  }

  active_bezier = trajectory.begin(); // ehnii bezier muruig songoj baina
  interrupt_counter = 0;
}

DiffRobot::~DiffRobot(){
  tcp_socket->destroy();
  delete tcp_socket;
  delete tcp_socket_ps;
  interrupt_data_file_log.close();
}

void DiffRobot::robot_config(string file_name){
  ifstream input_file(file_name, ios::in);

  if (!input_file.is_open()){
    cerr << "Robot config file opening problem occured!\n";
    exit(1);
  }
  double temp;

  input_file>>L;
  input_file>>M;
  input_file>>delta;
  input_file>>theta;

  input_file>>temp;
  position.setX(temp);
  input_file>>temp;
  position.setY(temp);

  set_theta(0);                               //Robotiin urd duguinii anhnii untsgiig 0 bolgono              
}

void DiffRobot::get_Bezier_trajectory(){
  // trajectory - g file-aas unshih function
  ifstream bezier_points_file("bezier_points.txt", ios::in);

  if (!bezier_points_file.is_open()){
    cerr << "File opening problem occured!\n";
    exit(1);
  }
  int num_of_curves;
  double x, y;
  Point p0,p1,p2,p3;

  bezier_points_file>>num_of_curves;
  for (int i=0; i<num_of_curves; i++){
    bezier_points_file>>x;
    bezier_points_file>>y;
    p0 = Point(x, y);

    bezier_points_file>>x;
    bezier_points_file>>y;
    p1 = Point(x, y);

    bezier_points_file>>x;
    bezier_points_file>>y;
    p2 = Point(x, y);

    bezier_points_file>>x;
    bezier_points_file>>y;
    p3 = Point(x, y);

    CubeBezier cube_bezier(p0, p1, p2, p3);

    trajectory.push_back(cube_bezier);
  }
}

void DiffRobot::interrupt_handler(InterruptData a){

  increase_interrupt_counter(a);

  if (interruptDatas.empty()){
    // first interrupt
    InterruptData a_1(A_DUGUI, start_time.tv_sec, start_time.tv_usec);
    InterruptData b_1(B_DUGUI, start_time.tv_sec, start_time.tv_usec);
    if (abs(a.value) == A_DUGUI){
      save_interrupt_data(b_1);
      save_interrupt_data(a_1);
    }
    else {
      save_interrupt_data(a_1);
      save_interrupt_data(b_1);
    }
    save_interrupt_data(a);
  }
  else {
    // not first interrupt
    InterruptData last_interrupt;
    last_interrupt = interruptDatas.back();
    if(abs(a.value) == A_DUGUI)
    {
      // urd duguin ontsgiig tootsoj set hiine
      double angle;
      angle = active_bezier->get_angle(time_add[index], M);
      set_theta(angle);
      index++;
    }

    if (abs(last_interrupt.value) == abs(a.value)){
      save_interrupt_data(a);
    }
    else {
      save_interrupt_data(a);
      calc_position();
      print_state();
      path_plan();
      //cout<<"SENDING SENDING"<<endl<<endl<<endl;
      //send_posAB();
      send_posAB_ps();
    }
  }
}

void DiffRobot::send_posAB(){
  // sending positions of wheel A and B
  string st;
  st = "1 ";
  Point a(get_Aposition());
  Point b(get_Bposition());
  st = st + a.convert_string() + " " + b.convert_string();

  try{
    *tcp_socket<<st;
  }
  catch (const libsocket::socket_exception& exc)
  {
    cerr << exc.mesg;
  }
}

void DiffRobot::send_posAB_ps(){
  string st;
  st = "1 ";
  Point a(get_Aposition());
  Point b(get_Bposition());
  st = st + a.convert_string() + " " + b.convert_string();
  try {
    st += "\r";
    tcp_socket_ps->send(st.c_str(), st.length());
  }
  catch(SocketException &e) {
    cerr <<"ERROR SENDING: "<< e.what() << endl;
  }
}

void DiffRobot::send_bezier_curves(){
  // sending read bezier curves
  string st = "2";
  vector<CubeBezier>::iterator it;
  for(it = trajectory.begin(); it != trajectory.end(); it++){
    st = st + " " + it->convert_string();
  }
  try{
    //cout<<st;
    *tcp_socket<<st;
  }
  catch (const libsocket::socket_exception& exc)
  {
    cerr << exc.mesg;
  }
}

void DiffRobot::send_bezier_curves_ps(){
  vector<CubeBezier>::iterator it;
  for(it = trajectory.begin(); it != trajectory.end(); it++){
    send_bezier_curve_ps(it);
  }
}

void DiffRobot::send_bezier_curve_ps(vector<CubeBezier>::iterator it){
  string st = "2";
  st = st + " " + it->convert_string();
  try {
    st+="\r";
    tcp_socket_ps->send(st.c_str(), st.length());
  }
  catch(SocketException &e) {
    cerr << e.what() << endl;
  }
}

void DiffRobot::save_interrupt_data(InterruptData a){
  interruptDatas.push_back(a);
  interrupt_data_file_log<<a.convert_string()<<endl;
}

void DiffRobot::calc_position(){
  /*
  interruptDatas dotor interruptuudiin tsuvaa baina.
  tsuvaanii format ni e.g: ABBBBBBBBA;
                            BAAAAAAAAAAAAAAAAAAAAAB;
                            BAB;
                            ABA;
                            ABBBBBBBBA;
  helbertei baina. Dund ni yamar negen baidlaar nogoo duguinii interrupt orj irehgui gej uzne.
  end A -- a duguinii interrupt
  B -- b duguinii interrupt
  */
  InterruptData first_intrpt, last_intrpt;
  first_intrpt = interruptDatas.front();
  last_intrpt = interruptDatas.back();
  if (check_format_interruptDatas()){
    // herev format ni zov baival
    vector<InterruptData>::iterator it;

    if (abs(first_intrpt.value) == A_DUGUI){
      // a dugui baival
      double time_delta = last_intrpt - first_intrpt;
      update_velA(last_intrpt.value*time_delta/abs(last_intrpt.value));

      for(it = interruptDatas.begin(); it != interruptDatas.end()-2; it++){
        time_delta = *(it+1) - *it;
        if (abs((it+1)->value) == abs(it->value)) {
          // hoyulaa B duguin interrupt baival ug hugatsaanii zavsart B duguin yavsan hurdiig tootsoolno.
          update_velB((it+1)->value*time_delta/abs((it+1)->value));
        }
        update_state(time_delta); // hurduudiig ashiglan bairshil, ontsogoo tootsno.
      }
    }
    else {
      // b dugui baival
      double time_delta = last_intrpt - first_intrpt;
      update_velB(last_intrpt.value*time_delta/abs(last_intrpt.value));

      for(it = interruptDatas.begin(); it != interruptDatas.end()-2; it++){
        time_delta = *(it+1) - *it;
        if (abs((it+1)->value) == abs(it->value)) {
          // hoyulaa A duguin interrupt baival ug hugatsaanii zavsart A duguin yavsan hurdiig tootsoolno.
          update_velA((it+1)->value*time_delta/abs((it+1)->value));
        }
        update_state(time_delta); // hurduudiig ashiglan bairshil, ontsogoo tootsno.
      }
    }
    InterruptData t1(*it), t2(*(it+1));
    interruptDatas.clear();
    interruptDatas.push_back(t1);
    interruptDatas.push_back(t2);
  }
  else {
    // interruptDatas vector buruu formattai baina
    cerr<<"\nINTERRUPT FORMAT IS WRONG.\n\n";
  }
}

void DiffRobot::update_state(double time_delta){
  // position, theta hoyriig update hiine
  // time_delta -- hugatsaa
  double omega = (velA_wheel - velB_wheel)/L; // rad/s negjtei
  theta += omega*time_delta;

  //cout<<"time delta: "<<time_delta<<endl;
  //cout<<"velA_wheel: "<<velA_wheel<<endl;
  //cout<<"velB_wheel: "<<velB_wheel<<endl;
  //cout<<"theta: "<<theta<<endl;

  // position-g tootsno
  if (velA_wheel == velB_wheel){
    // hoyor duguin hurd tentsuu tohioldold
    double pos_x, pos_y;
    pos_x = position.getX();
    pos_y = position.getY();

    pos_x += velA_wheel*cos(theta)*time_delta;
    pos_y += velA_wheel*sin(theta)*time_delta;
                                                      // pos_x, pos_y ni odoogiin bairshil, 
    position.set(pos_x, pos_y);
  }
  else {
    // hoyor duguin hurd yalgaatai tohioldold ergeh hodolgoon hiine
    double radius = L*(velA_wheel+velB_wheel)/(2*(velA_wheel-velB_wheel));
    //cout<<"radius: "<<radius<<endl;
    double pos_x, pos_y;
    pos_x = position.getX();
    pos_y = position.getY();
    //cout<<endl<<"before rotation"<<endl;
    //cout<<"pos_x: "<<pos_x<<endl;
    //cout<<"pos_y: "<<pos_y<<endl;
    Point ICC(pos_x-radius*sin(theta), pos_y+radius*cos(theta)); // theta ontsog radian hemjeeseer baina
    pos_x = ICC.getX();
    pos_y = ICC.getY();
    //cout<<endl<<"ICC x: "<<pos_x<<" ICC y: "<<pos_y<<" omega: "<<omega<<endl;
    //cout<<"omega*time_delta: "<<omega*time_delta<<endl;
    position.rotate_about(ICC, omega*time_delta);

    pos_x = position.getX();
    pos_y = position.getY();
    //cout<<endl<<"after rotation"<<endl;
    //cout<<"pos_x: "<<pos_x<<endl;
    //cout<<"pos_y: "<<pos_y<<endl;
  }
}

void DiffRobot::update_velA(double time_delta){
  velA_wheel = delta/time_delta;
}

void DiffRobot::update_velB(double time_delta){
  velB_wheel = delta/time_delta;
}

bool DiffRobot::check_format_interruptDatas(){
  InterruptData first_intrpt, last_intrpt;
  vector<InterruptData>::iterator it;

  first_intrpt = interruptDatas.front();
  last_intrpt = interruptDatas.back();

  if (abs(first_intrpt.value) == abs(last_intrpt.value)){
    int return_value = 1;
    for (it = interruptDatas.begin()+1; it != interruptDatas.end()-2; it++){
      if (abs(it->value) == abs(first_intrpt.value)) return_value = 0; // format aldaatai
    }
    return return_value;
  }
  else return 0;
}

Point DiffRobot::get_position(){
  return position;
}
double DiffRobot::get_theta(){
  return theta;
}
Point DiffRobot::get_Aposition(){
  Point p(L*sin(theta)/2, -L*cos(theta)/2);
  return position+p;
}
Point DiffRobot::get_Bposition(){
  Point p(-L*sin(theta)/2, L*cos(theta)/2);
  return position+p;
}


void * DiffRobot::driverReading(void *arg){
  DiffRobot *robot;
  robot = (DiffRobot *) arg;

  struct input_event ev;
  int fd, rd, size = sizeof (struct input_event);
  pthread_mutex_unlock(&writelock);
  //Open Device
  if ((fd = open(robot->rotary_driver, O_RDONLY)) == -1)
    cout<<robot->rotary_driver<<" is not a vaild device.\n";

  while (1){
    if ((rd = read(fd, &ev, size)) < size) pthread_exit(0);
    //rd = read(fd, &ev, size);
    if (ev.type == 2){
      pthread_mutex_lock(&writelock);
      // create interrupt data
      InterruptData a(ev.value, ev.time.tv_sec, ev.time.tv_usec);
      // interrupt handle
      //cout<<"event value: "<<ev.value<<endl;
      robot->interrupt_handler(a);

      pthread_mutex_unlock(&writelock);
    }
  }
  pthread_exit(0);
}

pthread_t * DiffRobot::get_driver_reading_thread(){
  return &driver_reading_thread;
}

void DiffRobot::print_state(){
  double x, y;
  Point temp;
  x = position.getX();
  y = position.getY();
  cout<<"x: "<<x<<" y: "<<y<<" theta: "<<theta<<endl;

  // print wheel A position
  temp = get_Aposition();
  x = temp.getX();
  y = temp.getY();
  cout<<"ax: "<<x<<" ay: "<<y<<endl;

  // print wheel B position
  temp = get_Bposition();
  x = temp.getX();
  y = temp.getY();
  cout<<"bx: "<<x<<" by: "<<y<<endl;
}


void DiffRobot::increase_interrupt_counter(InterruptData a){
  if (abs(a.value) == A_DUGUI){
    interrupt_counter++;
  }
}

void DiffRobot::path_plan(){
  /*if(is_necessary_replan()){
    interrupt_counter = 0;
    if(active_bezier->is_possible_replan(position, theta)){
      active_bezier->replan_curve(position, theta);
    }
    else
    {
      if(active_bezier!=trajectory.end()){
        active_bezier++;
        active_bezier->replan_curve(position, theta);
        active_bezier->calc_eq_times(time_add, delta);
        index  = 0;
      }
      else{
        // Bezier murui duussan
        cout<<"end of bezier curve\n\n" << endl;
      }
    }
    send_bezier_curve_ps(active_bezier);
  }*/
}

bool DiffRobot::is_necessary_replan(){
  if (interrupt_counter > NUM_OF_INT_TO_REPLAN){
    return 1;
  }
  else return 0;
}










/*---------------------------------------------------- Endees ehleed Namsraigiin bichse code bgaa -----------------------------------------------------*/

void DiffRobot::set_theta(double angle){
  std::fstream fs;
  double pwm_val;
  pwm_val = 850000+angle*9444;
  fs.open(pwm_path "/duty", std::fstream::out);
  fs << pwm_val;
  fs.close();
}


