
#include <iostream>
#include <math.h>
#include <string>
#include "point.h"

using namespace std;

Point::Point(){
  this->x = 0;
  this->y = 0;
}

Point::Point(const Point& temp){
  this->x = temp.x;
  this->y = temp.y;
}

Point::Point(double x, double y){
  this->x = x;
  this->y = y;
}

void Point::set(Point p){
  this->x = p.x;
  this->y = p.y;
}

void Point::set(double x, double y){
  this->x = x;
  this->y = y;
}

Point Point::get(){
  return *this;
}

void Point::show(){
  cout<<"("<<x<<", "<<y<<")"<<endl;
}

double Point::measure_dist(double x, double y){
  return sqrt((y-this->y)*(y-this->y)+(x-this->x)*(x-this->x));
}

double Point::measure_dist(Point a){
  return ((a.y-this->y)*(a.y-this->y)+(a.x-this->x)*(a.x-this->x));
}

double Point::measure_dist(double x, double y, double px, double py){
  double x1=(px-this->x)/2;
  double y1=(py-this->y)/2;
  if(x1<0){
    x1=px+x1*(-1);
  }
  else{
    x1+=this->x;
  }
  if(y1<0){
    y1=py+y1*(-1);
  }else{
    y1+=this->y;
  }
  return sqrt((x-x1)*(x-x1)+(y-y1)*(y-y1));
}
void Point::setX(double x){this->x = x;}
void Point::setY(double y){this->y = y;}

double Point::getX(){return this->x;}
double Point::getY(){return this->y;}
double Point::get_theta(){
  if (this->get_norm_squared() == 0) return 0;
  else {
    double theta = acos(this->x/sqrt(this->get_norm_squared()));
    return theta;
  }
}

double Point::dotProduct(Point p){
  return  this->x * p.x + this->y * p.y;
}

double Point::get_norm_squared(){
  return this->dotProduct(*this);
}


double Point::crossProductZ(Point p){
  double s = this->x * p.y - this->y * p.x;
  return s;
}

void Point::rotate_about(Point p, double alpha){
  /* tsegiin p tsegiin huvid alpha ontsgoor eruuleh function */

  // p tseg deer ehtei coordinatiin systemd shiljine
  //cout<<"\nrotate about function\n\n"<<"alpha: "<<alpha<<endl;
  Point a;
  a.set(*this-p);

  // alpha ontsgoor erguulne
  double a_x, a_y;
  a_x = a.getX();
  a_y = a.getY();
  //cout<<"x: "<<this->getX()<<" y: "<<this->getY()<<endl;
  //cout<<"p_x: "<<p.getX()<<" p_y: "<<p.getY()<<endl;
  //cout<<"a_x: "<<a.getX()<<" a_y: "<<a.getY()<<endl;
  Point b(cos(alpha)*a_x - sin(alpha)*a_y, sin(alpha)*a_x + cos(alpha)*a_y);
  //cout<<"b_x: "<<b.getX()<<" b_y: "<<b.getY()<<endl;
  // huuchin coordinatiin systemd shiljine
  b=p+b;

  this->set(b); // bairlaliig shineer update hiine
}

string Point::convert_string(){
  string x_str = to_string(x);
  string y_str = to_string(y);
  return x_str + " " + y_str;
}

double Point::operator*(Point p){
  return this->dotProduct(p);
}
Point Point::operator*(double d){
  Point result;
  result.setX(this->getX()*d);
  result.setY(this->getY()*d);
  return result;
}
Point Point::operator*(int d){
  Point result;
  result.setX(this->getX()*d);
  result.setY(this->getY()*d);
  return result;
}
Point Point::operator/(double d){
  Point result;
  result.setX(this->getX()/d);
  result.setY(this->getY()/d);
  return result;
}


Point Point::operator+(Point p){
  Point result;

  result.setX(this->x + p.x);
  result.setY(this->y + p.y);

  return result;
}

Point Point::operator-(Point p){
  Point result;

  result.setX(this->x - p.x);
  result.setY(this->y - p.y);

  return result;
}

Point Point::operator+=(Point p){
  this->setX(this->x + p.x);
  this->setY(this->y + p.y);

  Point result;
  result.setX(this->x);
  result.setY(this->y);
  return result;
}

Point Point::operator-=(Point p){
  this->setX(this->x - p.x);
  this->setY(this->y - p.y);

  Point result;
  result.setX(this->x);
  result.setY(this->y);
  return result;
}
