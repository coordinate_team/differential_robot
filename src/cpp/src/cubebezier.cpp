
#include "cubebezier.h"

CubeBezier::CubeBezier(Point P0, Point P1, Point P2, Point P3){
  this->P0 = P0;
  this->P1 = P1;
  this->P2 = P2;
  this->P3 = P3;
  update_ABCD();
  calc_length();
}

void CubeBezier::update_control_points(Point P0, Point P1, Point P2, Point P3){
  this->P0 = P0;
  this->P1 = P1;
  this->P2 = P2;
  this->P3 = P3;
  update_ABCD();
}

void CubeBezier::update_ABCD(){
  A = P3 - P2 * 3 + P1 * 3 - P0;
  B = P2 * 3 - P1 * 6 + P0 * 3;
  C = (P1 - P0) * 3;
  D = P0;
}

std::string CubeBezier::convert_string(){
  std::string st;
  st = P0.convert_string() + " " + P1.convert_string() + " " + P2.convert_string() + " " + P3.convert_string();
  return st;
}

double CubeBezier::get_nearest_point_parameter(Point p){
  // DUTUU
  // t parameter-iig butsaah function
  double result = 0;
  for(double tt=0;tt<=1.0;tt+=0.00002) {
    if(p.measure_dist(this->get_point_on_curve(tt)) < p.measure_dist(this->get_point_on_curve(result)))
      result = tt;
  }
  return result;
}

Point CubeBezier::get_nearest_point(Point p){
  double t_param = this->get_nearest_point_parameter(p);
  return get_point_on_curve(t_param);
}
Point CubeBezier::get_A_param(){
  return A;
}

Point CubeBezier::get_B_param(){
  return B;
}

Point CubeBezier::get_C_param(){
  return C;
}

Point CubeBezier::get_D_param(){
  return D;
}


Point CubeBezier::get_point_on_curve(double t_param){
  // t parametert hargalzah tsegiin butsaah function
  Point result = A * pow3(t_param) + B * pow2(t_param) + C * t_param + D;
  return result;
}

void CubeBezier::replan_curve(Point position, double theta){
  // shalgah shaardlagatai
  /*
    bezier muruig shineer baiguulah function
    ----------------------------------------
    position -- robotiin bairlal, bezier muruigaas gadna bairlana
    theta -- hurdnii chigleliin x-axis-tai uusgeh ontsog (radian)
  */

  double t_param = this->get_nearest_point_parameter(position);
  //cout<<"t-parameter: "<<t_param<<endl;
  Point Q_hat = this->get_point_on_curve(t_param);

  Point B_hat = (A * t_param * 3 + B)*pow2(1-t_param);
  Point C_hat = (A * 3 * pow2(t_param) + B * t_param * 2 + C)*(1-t_param);
  Point D_hat = A * pow3(t_param) + B * pow2(t_param) + C * t_param + D;

  Point P1_hat = C_hat/3 + D;
  Point P2_hat = B_hat/3 + C_hat * 2/3 + D_hat * 2 - Q_hat;

  P1_hat = P1_hat - Q_hat + position; // buruu baij magadgui

  // P1_hat -- tsegiin rotate hiih uildel
  Point temp = P1_hat - position; // QP1_hat vector
  double theta_temp = temp.get_theta(); // x-axis-tai uusgeh ontsog
  temp.rotate_about(Point(0,0), theta - theta_temp);
  P1_hat = temp + position;

  this->update_control_points(position, P1_hat, P2_hat, this->P3);

}



/*---------------------------Namsraikhuu nemev------------------------------*/

double CubeBezier::calc_length(){
  int n=2000, i; 
  double a=0,b=1,h, sum=0;
  double x[n+ 1], y[n+1];
  h = (b-a)/n;
  for(i=0; i<n+1; i++){
    x[i]=a+i*h;
    y[i]=(sqrt(9*(A*A)*(x[i]*x[i]*x[i]*x[i])+12*(A*B)*(x[i]*x[i]*x[i])+(4*(B*B)+6*(A*C))*(x[i]*x[i])+4*(B*C)*x[i]+C*C));
  }
  //cout << y[i] << endl;
  for(i=0; i<n; i+=2){
    sum=sum+4*y[i];
  }
  for(i=2; i<n-1; i+=2){
    sum=sum+2*y[i];
  }
  length=h/3*(y[0]+y[n]+sum);
  return length;
}

void CubeBezier::calc_eq_times(double * time_add, double delta){
	int num_of_step;
	num_of_step = this->length/delta;
  	delete[] time_add;
  	time_add = new double[num_of_step];
   	calc_runge_kutta(time_add, 0, 0, delta, num_of_step);
}

void CubeBezier::calc_runge_kutta (double y[], int x0, int y0, double h, int N){
    double k1, k2, k3, k4;
    y[0] = y0;
    for(int i=0; i<N; i++){
      k1 = h * func_of_runge(y[i]);
      k2 = h * func_of_runge(y[i] + k1/2); 
      k3 = h * func_of_runge(y[i] + k2/2);
      k4 = h * func_of_runge(y[i] + k3);
      y[i+1] = y[i] + (k1+2*k2+2*k3+k4)/6;
    }
}

double CubeBezier::func_of_runge(double arg){
    double arr_time;
    arr_time = 1/(sqrt(9*(A*A)*(arg*arg*arg*arg)+12*(A*B)*(arg*arg*arg)+(4*(B*B)+6*(A*C))*(arg*arg)+4*(B*C)*arg+C*C));
    //arr_time=0.0025;
    return arr_time;
}
double CubeBezier::get_angle(double t_param, double M){
  // radius olno
  double A_x = A.getX();
  double B_x = B.getX();
  double C_x = C.getX();
  double A_y = A.getY();
  double B_y = B.getY();
  double C_y = C.getY();

	double R, omega, norm_C, angle;
	R = 4*(((3*A_x*t_param +B_x)*(3*A_x*t_param +B_x))+((3*A_y*t_param+B_y)*(3*A_y*t_param+B_y)));
	R = sqrt(pow(R,3));
	R = R / fabs(12*A_y*B_x-12*A_x*B_y);
	omega = 6*t_param*t_param*(A_y*B_x - A_x*B_y)+6*t_param*(A_y*C_x - A_x*C_y)+2*(B_y*C_x - B_x*C_y);
	norm_C = 9*A.dotProduct(A)*pow4(t_param)+12*A.dotProduct(B)*pow3(t_param)+(4*B.dotProduct(B)+6*A.dotProduct(C))*pow2(t_param)+4*B.dotProduct(C)*t_param+C.dotProduct(C);
	norm_C = sqrt(norm_C);
	omega = omega/norm_C;
	angle = pi/2-R/M;
  return angle;
}

