
#ifndef CUBEBEZIER_H_
#define CUBEBEZIER_H_
#include "point.h"
#include <string>
#include <fstream>
#include <math.h>

#define pow1(x) (x)
#define pow2(x) (x)*(x)
#define pow3(x) (x)*(x)*(x)
#define pow4(x) (x)*(x)*(x)*(x)
#define pow5(x) (x)*(x)*(x)*(x)*(x)
#define pow6(x) (x)*(x)*(x)*(x)*(x)*(x)
#define pi 3.1416

class CubeBezier
{
  Point P0, P1, P2, P3, A, B, C, D;
 public:
  CubeBezier(Point P0, Point P1, Point P2, Point P3);
  std::string convert_string();
  double get_nearest_point_parameter(Point p);
  Point get_nearest_point(Point p);
  Point get_point_on_curve(double t_param);
  void update_ABCD();
  void update_control_points(Point P0, Point P1, Point P2, Point P3);
  void replan_curve(Point position, double theta);
  Point get_A_param();
  Point get_B_param();
  Point get_C_param();
  Point get_D_param();

  /*---------------------------------Namsraikhuu nemev----------------*/
  double length;
  double calc_length();
  void calc_eq_times(double * time_add, double delta);
  void calc_runge_kutta(double y[], int x0, int y0, double h, int N);
  double func_of_runge(double);
  double get_angle(double t_param, double M);


};
#endif /* CUBEBEZIER_H_ */
