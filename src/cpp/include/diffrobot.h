#ifndef DIFFROBOT_H_
#define DIFFROBOT_H_

#define A_DUGUI 1
#define B_DUGUI 2

#include <vector>
#include <pthread.h>
#include <sys/time.h>
#include <fstream>

#include "point.h"
#include "interruptdata.h"
#include "cubebezier.h"
#include "inetclientstream.hpp"
#include "practical_socket.h"

#define NUM_OF_INT_TO_REPLAN 499

using namespace std;


class DiffRobot
{
  struct timeval start_time; // robotiin object baiguulagdah uyiin hugatsaag hadgalah huvisagch

  pthread_t driver_reading_thread;
  char rotary_driver[100];

  double L, M, delta, theta; // L hoid tenhlegiin urt, delta - rotary encoderiin alhamiin hemjee, theta - ontsog
  Point position; // robotiin bairlal

  double velA_wheel, velB_wheel;

  vector<InterruptData> interruptDatas; // interrupt datas
  InterruptData intData, lastIntData; //

  vector<CubeBezier> trajectory; // trajectory
  vector<CubeBezier>::iterator active_bezier;
  int interrupt_counter;

  libsocket::inet_stream* tcp_socket; // tcp socket (data-gaa ilgeehed ashiglana)
  TCPSocket *tcp_socket_ps;

  ofstream interrupt_data_file_log;

  void save_interrupt_data(InterruptData a); // interrupt-datag hadgalah function
  void calc_position(); // bairlaliig interrupt-datagiin utguudiig ashiglan tootsooloh function
  void update_state(double time_delta);
  bool check_format_interruptDatas();
  void update_velA(double time_delta);
  void update_velB(double time_delta);
  static void* driverReading(void *arg);
  void send_posAB();
  void send_posAB_ps();
  void send_bezier_curves();
  void send_bezier_curves_ps();
  void send_bezier_curve_ps(vector<CubeBezier>::iterator it);
  void robot_config(string file_name);

  void increase_interrupt_counter(InterruptData a);
  void path_plan();
  bool is_necessary_replan(); // barilal bolon theta-giin medeelliig ashiglan trajector shineer baiguulah shaardlagatai esehiig shalgah function

  public:
  DiffRobot();
  DiffRobot(string ip_addr, string port);
  ~DiffRobot();
  void get_Bezier_trajectory();
  void interrupt_handler(InterruptData a); // interrupt ireh uyd duudagdah function
  Point get_position();
  double get_theta();
  Point get_Aposition();
  Point get_Bposition();
  pthread_t * get_driver_reading_thread();
  void print_state();


  /*-------------------------------------------Namsraikhuu-----------------------------------*/
  int index;
  double *time_add;
  /*void load_runge_kutta();                                                        //runge_kutta-g funktsiig beldeh funkts
  double* runge_kutta (double y[], int x0, int y0, double h, int N);                 //runge_kutta-g idevhjuuleh
  double func_runge(double arg);                                                  //runge_kutta-g bodoh funkts
  double find_integral();                                                         //integral oloh funkts
  double func_integral(double index_suugii);                                      //integral boduulah funkts
  void compare_coordinate(Point rA);                                              //Odoogiin bairlaliig Daalgavaraiin bairlaltai jishih 
  double find_radius(double tt);                                                  //radius oloh funkts
  void calc_rotate_angle(double tt);                                              //erguuleh funkts*/
  void set_theta(double angle);

};

  /*
  void calc_coordinate(double tt, int value, unsigned int sec, unsigned int usec)               //Coordinate oloh gol function
  {
    //  cout << sec<< " " << usec << endl;
    if(value + current1.value == 0) {///?????
      current1.value *= -1;
      return;
    }
    current.value = value;
    current.sec = sec;
    current.usec = usec;
    if(current.value%2==0) current.t1 = current-lastB;
    else current.t1 = current-lastA;
    current.t2 = current-current1;

    if(fabs(current.value)==fabs(current1.value)){
      backup[m] = current;
      m++;
    }
    else {
      start_calculating();
      //correct((rA_now+rB_now)*0.5, tt);//trajector zasah heseg
    }


    current1 = current;
    if(current.value%2==0)
      lastB = current;
    else lastA = current;

    if(m>20) {
      current.t1 = inf;
      //current.t2 = inf;
      start_calculating();
      }
  }
  void start_calc_error(){
    double velo;

  }

  void start_calculating(){
    double velo;
    int i;
    for(int i=0; i<m; i++) {
      if(!(current.value%2==0)) {
	double x = backup[i].t2/backup[i].t1;
	double y = backup[i].t2/current.t1;
	if(backup[i].value<0) x = -x;
	if(current.value<0) y = -y;
	theta += (x-y)*delta/L;
	velo = current.get_velocity(delta);
	rA_now.x = rA_now.x - velo*sin(theta)*backup[i].t2;
	rA_now.y = rA_now.y + velo*cos(theta)*backup[i].t2;
      }
      else {
	double x = backup[i].t2/backup[i].t1;
	double y = backup[i].t2/current.t1;
  //cout << x << " " << y << endl;
	if(backup[i].value<0) x = -x;
	if(current.value<0) y = -y;
	theta -= (x-y)*delta/L;
	velo = backup[i].get_velocity(delta);
	rA_now.x = rA_now.x - velo*sin(theta)*backup[i].t2;
	rA_now.y = rA_now.y + velo*cos(theta)*backup[i].t2;
      }

      rB_now.x = rA_now.x + L*cos(theta);
      rB_now.y = rA_now.y + L*sin(theta);

      myfile << rA_now.x << " " << rA_now.y <<" " << rB_now.x << " " << rB_now.y << endl;
      //compare_coordinate(rA_now);
      index++;
      //cout << rA_now.x << " " << rA_now.y <<" " << rB_now.x << " " << rB_now.y << endl;
      //sendData_namsraikhuu(rA_now, rB_now);
      double x, y;
      Point realA, realB;
      realA = bez[n].getPointA(tt, L);
      if(backup[i].value==1||1){
	//myfile2 << rA_now.x<< " " << rA_now.y<< " " << realA.x << " " << realA.y << endl;
	//TCP_send(rA_now, rB_now);
	// cout << bez[n].p[0].x << " * " << bez[n].p[0].y << endl;
      }
    }

    m=0;
    backup[m++]= current;
  }

void compare_coordinate(Point rA){
	Point mission;
	double mission_distance;
	mission = P0*(1-time_add[index])*(1-time_add[index])*(1-time_add[index])+P1*3*(1-time_add[index])*(1-time_add[index])*time_add[index]+P2*3*(1-time_add[index])*time_add[index]*time_add[index]+P3*time_add[index]*time_add[index]*time_add[index];
	calc_rotate_angle(time_add[index]); //  duguigaa erguuldeg function
}
  double func(double arg){
	double arr_time;
	arr_time = 1/(sqrt(9*(A*A)*(arg*arg*arg*arg)+12*(A*B)*(arg*arg*arg)+(4*(B*B)+6*(A*C))*(arg*arg)+4*(B*C)*arg+C*C));
	//arr_time=0.0025;
	return arr_time;
}
int * runge_kutta (int y[], int x0, int y0, double h, int N){
	double k1, k2, k3, k4;
	//double y[N];
	//double x[N];
	//x[0] = x0;
	y[0] = y0;
	for(int i=0; i<N; i++){
		//x[i+1] = x[i]+h;
		k1 = h * func(y[i]);
		k2 = h * func(y[i] + k1/2);
		k3 = h * func(y[i] + k2/2);
		k4 = h * func(y[i] + k3);
		y[i+1] = y[i] + (k1+2*k2+2*k3+k4)/6;
	}
	return y;
}
double find_integral(){
	double cur_val, whole_val=0;
	for(float index_suugii=0; index_suugii<=1; index_suugii+=0.0005){
		cur_val = (sqrt(9*(A*A)*(index_suugii*index_suugii*index_suugii*index_suugii)+12*(A*B)*(index_suugii*index_suugii*index_suugii)+(4*(B*B)+6*(A*C))*(index_suugii*index_suugii)+4*(B*C)*index_suugii+C*C));
		whole_val= whole_val+cur_val;
	}
  cout << "Testing: find_integral" << endl;
	return whole_val;
}
void Read_param(){
  cout << "Testing1" << endl;
	param_file.open("include/bezier_murui.txt");
	while(param_file >> this->P0.x >> this->P0.y >> this->P1.x >> this->P1.y >> this->P2.x >> this->P2.y >> this->P3.x >> this->P3.y){
	cout << "Read from file" << endl;
	}
	calc_param();
}
void calc_param(){
	A = P3-P2*3+P1*3-P0;
	B = P2*3-P1*6+P3*3;
	C = P1*3-P0*3;
	D = P0;
  cout << "Testing: calc_param" << endl;
  load_runge_kutta();
}
void load_runge_kutta(){
  double zai;
  int *result;
  zai = find_integral();
  step=zai/delta;
  cout << step << endl;
  cout << zai << endl;
  time_add = new int [step];
  //time_add = (int *) malloc(step * sizeof(double));
  cout << "Testing: load_runge_kutta" << endl;
  result = runge_kutta(time_add, 0, 0, 0.4, step);
}


double find_radius(double tt){
  double R;
	R = 4*(((3*A.x*tt +B.x)*(3*A.x*tt +B.x))+((3*A.y*tt+B.y)*(3*A.y*tt+B.y)));
	R = (3*A.x*tt*tt + 2*B.x*tt + C.x) * (3*A.x*tt*tt + 2*B.x*tt + C.x) + (3*A.y*tt*tt + 2*B.y*tt + C.y) * (3*A.y*tt*tt + 2*B.y*tt + C.y);
	R = sqrt(pow(R,3));
	R = R / abs(2*(3*tt*tt*(A.y*B.x - A.x*B.y)+ 3*tt*(A.y*C.x-A.x*C.y)+(B.y*C.x*B.x-C.y)));
	return R;
}

void calc_rotate_angle(double tt){

	double angle;
	double Radius;
  Point Z;
  Z=P0*(1-time_add[index+1])*(1-time_add[index+1])*(1-time_add[index+1])+P1*3*(1-time_add[index+1])*(1-time_add[index+1])*time_add[index+1]+P2*3*(1-time_add[index+1])*time_add[index+1]*time_add[index+1]+P3*time_add[index+1]*time_add[index+1]*time_add[index+1];
  Radius = find_radius(tt);
	angle = pi/2 -atan(Radius/M);
	if(rA_now.x<Z.x)
		angle= -angle;
	rotate_wheel(angle);
}

void rotate_wheel(double angle){
  int pwm_val;
	pwm_val = 850000+angle*9444;
	set_pwm(pwm_val);
}

void config_pwm(){
	std::fstream fs;
	fs.open(pwm_path "/period", std::fstream::out);
	fs << "3000000";
	fs.close();
}

void set_pwm(int pwm_val){
	std::fstream fs;
	fs.open(pwm_path "/duty", std::fstream::out);
	fs << pwm_val;
	fs.close();
}

};
*/

#endif
