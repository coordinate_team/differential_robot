/*
 * Point.h
 *
 *  Created on: Jan 8, 2013
 *      Author: nanzaa
 */

#ifndef POINT_H_
#define POINT_H_
#include <string>

class Point{

  double x, y;
  public:
  Point();
  Point(const Point& temp);
  Point(double x, double y);
  void set(Point p);
  void set(double x, double y);
  Point get();
  void show();
  double measure_dist(double x, double y);
  double measure_dist(Point a);
  double measure_dist(double x, double y, double px, double py);
  void setX(double x);
  void setY(double y);
  double getX();
  double getY();
  double get_theta(); // x - axis -tai uusgeh ontsgiig butsaana (radian)
  double dotProduct(Point p);
  double get_norm_squared();
  double crossProductZ(Point p);
  void rotate_about(Point p, double alpha); // alpha erguuleh ontsgiin hemjee
  std::string convert_string();
  double operator*(Point p);
  Point operator*(double d);
  Point operator*(int d);
  Point operator/(double d);
  Point operator+(Point p);
  Point operator-(Point p);
  Point operator+=(Point p);
  Point operator-=(Point p);
};


#endif /* POINT_H_ */
