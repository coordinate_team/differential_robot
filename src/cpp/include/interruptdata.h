/*
 * TrajectoryPoint.h
 *
 *  Created on: Jan 20, 2013
 *      Author: nanzaa
 */

#ifndef INTERRUPTDATA_H_
#define INTERRUPTDATA_H_

#include <string>


class InterruptData
{

 public:
  int value;  // -1, -2, 1, 2 -- sorog ni hoishoo, 1 ni A dugui, 2 ni B dugui
  unsigned int sec, usec;
  double t1,t2;// interrupt hoorondiin hugastaa t1-umnuh ijil interrupt t2-yag umnuh interrupt
  InterruptData();
  InterruptData(int value, int sec, int usec);
  InterruptData(const InterruptData &a);

  double operator- (InterruptData p);

  std::string convert_string();
/*
  double get_velocity(double delta){
    if(fabs(this->value)==1)
      return (fabs(this->value)/(this->value))*delta/(this->t1);
    else
      return (fabs(this->value)/(this->value))*delta/(this->t1);
  }
*/
};
#endif /* INTERRUPTDATA_H_ */
