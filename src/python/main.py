import socket
import sys
import random

from pyqtgraph.Qt import QtGui, QtCore
from PyQt4 import QtNetwork
import pyqtgraph as pg
import numpy as np


class Server(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(Server, self).__init__(parent)

        #w = QtGui.QMainWindow()
        cw = pg.GraphicsLayoutWidget()

        statusLabel = QtGui.QLabel()

        #self.statusBar().showMessage('Ready')

        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('Statusbar')

        self.show()
        #self.resize(400,600)
        self.setCentralWidget(cw)
        p = cw.addPlot(row=0, col=0)

        self.tcpServer = QtNetwork.QTcpServer(self)
        if not self.tcpServer.listen():
            QtGui.QMessageBox.critical(self, "Fortune Server",
                    "Unable to start the server: %s." % self.tcpServer.errorString())
            self.close()
            return

        self.statusBar().showMessage("The server is running on port %d.\nRun the "
                "Fortune Client example now." % self.tcpServer.serverPort())

        #statusLabel.setText("The server is running on port %d.\nRun the "
        #        "Fortune Client example now." % self.tcpServer.serverPort())



        self.fortunes = (
                "You've been leading a dog's life. Stay off the furniture.",
                "You've got to think about tomorrow.",
                "You will be surprised by a loud noise.",
                "You will feel hungry again in another hour.",
                "You might have mail.",
                "You cannot kill time without injuring eternity.",
                "Computers are not intelligent. They only think they are.")

        #quitButton.clicked.connect(self.close)
        self.tcpServer.newConnection.connect(self.sendFortune)



    def sendFortune(self):
        block = QtCore.QByteArray()
        out = QtCore.QDataStream(block, QtCore.QIODevice.WriteOnly)
        out.setVersion(QtCore.QDataStream.Qt_4_0)
        out.writeUInt16(0)
        fortune = self.fortunes[random.randint(0, len(self.fortunes) - 1)]

        try:
            # Python v3.
            fortune = bytes(fortune, encoding='ascii')
        except:
            # Python v2.
            pass

        out.writeString(fortune)
        out.device().seek(0)
        out.writeUInt16(block.size() - 2)

        clientConnection = self.tcpServer.nextPendingConnection()
        clientConnection.disconnected.connect(clientConnection.deleteLater)

        clientConnection.write(block)
        clientConnection.disconnectFromHost()


if __name__ == '__main__':

    import sys

    app = QtGui.QApplication(sys.argv)
    server = Server()
    random.seed(None)
    sys.exit(app.exec_())
