
from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg

from PyQt4 import QtNetwork

import collections
import random
import time
import math
import numpy as np


SAMPLE_NUM = 100 # irj baigaa SAMPLE_NUM shirheg tseguudees negiig ni plot hiine
SERVERPORT = 5005

class DynamicPlotter():

    def __init__(self, sampleinterval=0.1, timewindow=10., size=(600,350)):
        # Data stuff
        self._interval = int(sampleinterval*500)
        self._bufsize = int(timewindow/sampleinterval)
        self.raw_databuffer = []
        self.databuffer = collections.deque()
        #self.wheel_A_buffer = collections.deque()
        #self.wheel_B_buffer = collections.deque()
        self.sampling_number = 1 # anhnii utga 1 baih yostoi
        #self.x = np.linspace(-timewindow, 0.0, self._bufsize)
        #self.y = np.zeros(self._bufsize, dtype=np.float)
        # PyQtGraph stuff
        self.app = QtGui.QApplication([])
        self.plt = pg.plot(title='Dynamic Plotting with PyQtGraph')
        self.plt.resize(*size)
        self.vwbox = self.plt.getViewBox()
        self.vwbox.setAspectLocked()
        self.plt.setXRange(-2000, 2000, padding=0)
        self.plt.setYRange(-2000, 2000, padding=0)
        self.plt.showGrid(x=True, y=True)
        self.plt.setLabel('left', 'y', 'mm')
        self.plt.setLabel('bottom', 'x', 'mm')
        #self.curve = self.plt.plot(self.x, self.y, pen=(255,0,0))
        #self.plt.plot(x=[2, 3, 4], y=[2, 3.5, 4], pen=None, symbol='o')

        # QTimer
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.updateplot)
        self.timer.start(self._interval)
        # QTcpServer
        self.tcpServer = QtNetwork.QTcpServer()
        if not self.tcpServer.listen(port = SERVERPORT):
            print "Error server can't start"
            return
        print "The server is running on port %d.\n" % self.tcpServer.serverPort()
        print "Run Client example now.\n"

    	self.tcpServer.newConnection.connect(self.get_tcp_data)


    def getdata(self):
        frequency = 0.5
        noise = random.normalvariate(0., 1.)
        new = 10.*math.sin(time.time()*frequency*2*math.pi) + noise
        return new
    def get_tcp_data(self):
        self.clientConnection = self.tcpServer.nextPendingConnection()
        #clientConnection.disconnected.connect(clientConnection.deleteLater)
        self.clientConnection.readyRead.connect(self.save_data)

    def save_data(self):
        msg = self.clientConnection.readAll()
        print "received data: ", msg
        msg=str(msg)
        sarray = msg.split()
        self.raw_databuffer = self.raw_databuffer + sarray
        #self.databuffer = self.databuffer.append(sarray)
        #print sarray
        

    def updateplot(self):
        #self.databuffer.append( self.getdata() )
        #self.y[:] = self.databuffer
        #self.curve.setData(self.x, self.y)
        #self.app.processEvents()
        while len(self.raw_databuffer)>0:
            if self.raw_databuffer[0] == '1':
                if len(self.raw_databuffer)>4:
                    sarray = self.raw_databuffer[0:5]
                    del self.raw_databuffer[0:5]
                    self.databuffer.append(sarray)
                else:
                    break
            elif self.raw_databuffer[0] == '2':
                if len(self.raw_databuffer)>8:
                    sarray = self.raw_databuffer[0:9]
                    del self.raw_databuffer[0:9]
                    self.databuffer.append(sarray)
                else:
                    break
            else:
                print "SEPARATING IF: tcp data is not starting with '1' or '2'\n"
                print len(self.raw_databuffer)
                a = self.raw_databuffer.pop()
                print "element is:", a
        

        while len(self.databuffer)>0:
            sarray = self.databuffer.popleft()
            if sarray[0] == '1':
                self.sampling_number=self.sampling_number-1
                if self.sampling_number == 0:
                    self.add_AB_wheel_positions(sarray[1:5])
                    self.sampling_number = SAMPLE_NUM
            elif sarray[0] == '2':
                self.add_bezier_curve(sarray[1:9])
            else:
                print "PLOTTING IF: tcp data is not starting with '1' or '2'\n"



    def add_AB_wheel_positions(self, sarray):
        #print "add AB wheel positions", sarray
        Ax = float(sarray[0])
        Ay = float(sarray[1])
        Bx = float(sarray[2])
        By = float(sarray[3])
        #self.wheel_A_buffer.append([Ax, Ay])
        #self.wheel_B_buffer.append([Bx, By])
        self.plt.plot(x=[Ax], y=[Ay], pen=(255,0,0), symbol='o')
        self.plt.plot(x=[Bx], y=[By], pen=(255,0,0), symbol='x')
    def add_bezier_curve(self, sarray):
        print "add bezier curve", sarray

    def run(self):
        self.app.exec_()

if __name__ == '__main__':

    m = DynamicPlotter(sampleinterval=0.05, timewindow=10.)
    m.run()

