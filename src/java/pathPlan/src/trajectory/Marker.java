package trajectory;

import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;

public class Marker {

	static final double radius = 5;

	Point2D.Double center;
	Ellipse2D.Double circle;

	public Marker(Point2D.Double control) {
		center = control;
		circle = new Ellipse2D.Double(control.getX() - radius, control.getY()
				- radius, 2 * radius, 2 * radius);
	}

	public void draw(Graphics2D g2D) {
		g2D.draw(circle);
	}

	public Point2D.Double getCenter() {
		return center;
	}

	public boolean contains(Point2D.Double p) {
		return contains(p.getX(), p.getY());
	}

	public boolean contains(double x, double y) {
		return circle.contains(x, y);
	}

	public double distance(double x, double y) {
		double dist = Math.sqrt(Math.pow(center.getX() - x, 2)
				+ Math.pow(center.getY() - y, 2));
		return dist;
	}

	public void setLocation(double x, double y) {
		center.x = x;
		center.y = y;
		circle.x = x - radius;
		circle.y = y - radius;
	}

	public void setLocation(Point2D.Double p) {
		center.x = p.x;
		center.y = p.y;
		circle.x = p.x - radius;
		circle.y = p.y - radius;
	}
}
