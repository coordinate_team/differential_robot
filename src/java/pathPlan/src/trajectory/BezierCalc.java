package trajectory;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;

public class BezierCalc {
	Point2D.Double A, B, C, D;
	
	public double M = 1, L = 1;
	public double delta = 1;

	public BezierCalc(Point2D.Double p0, Point2D.Double p1, Point2D.Double p2,
			Point2D.Double p3) {
		double ax = p3.x - 3 * p2.x + 3 * p1.x - p0.x;
		double ay = p3.y - 3 * p2.y + 3 * p1.y - p0.y;
		this.A = new Point2D.Double(ax, ay);

		double bx = 3 * (p2.x - 2 * p1.x + p0.x);
		double by = 3 * (p2.y - 2 * p1.y + p0.y);
		this.B = new Point2D.Double(bx, by);

		double cx = 3 * (p1.x - p0.x);
		double cy = 3 * (p1.y - p0.y);
		this.C = new Point2D.Double(cx, cy);

		double dx = p0.x;
		double dy = p0.y;
		this.D = new Point2D.Double(dx, dy);
	}
	
	public void setML(double M, double L) {
		this.M = M;
		this.L = L;
	}

	public Point2D.Double rtA(double t) {
		double x = ((A.x * t + B.x) * t + C.x) * t + D.x;
		double y = ((A.y * t + B.y) * t + C.y) * t + D.y;
		return new Point2D.Double(x, y);
	}

	public void drawLinesCD(Graphics2D g2D ,double L, double  M, double delta){
		Point2D.Double sPA = rtA(0);
		Point2D.Double sPB = rtB(0,L);
		
		double k = M/L;
		
		Point2D.Double sPD = new Point2D.Double(
				sPA.getX()-k*(sPB.getY()-sPA.getY()),
				sPA.getY()+k*(sPB.getX()-sPA.getX())
				);
		Point2D.Double sPC = new Point2D.Double(
				sPB.getX()-k*(sPB.getY()-sPA.getY()),
				sPB.getY()+k*(sPB.getX()-sPA.getX())
				);
		
		Point2D.Double ePA = null;
		Point2D.Double ePB = null;
		
		
		
		
		double t;
		for(t = delta; t<= 1; t+=delta)
		{
			ePB = rtB(t,L);
			ePA = rtA(t);
			
			Point2D.Double ePD = new Point2D.Double(
					ePA.getX()-k*(ePB.getY()-ePA.getY()),
					ePA.getY()+k*(ePB.getX()-ePA.getX())
					);
			Point2D.Double ePC = new Point2D.Double(
					ePB.getX()-k*(ePB.getY()-ePA.getY()),
					ePB.getY()+k*(ePB.getX()-ePA.getX())
					);
			
			g2D.drawLine((int)((sPD.x+sPC.x)/2), (int)(sPD.y+sPC.y)/2, (int)(ePD.x+ePC.x)/2, (int)(ePD.y+ePC.y)/2);
			//g2D.drawLine((int)sPC.x, (int)sPC.y, (int)ePC.x, (int)ePC.y);
			//g2D.drawLine((int)sPD.x, (int)sPD.y, (int)ePD.x, (int)ePD.y);
			
			sPD = ePD;
			sPC = ePC;			
		}
		if(t!=1){
			ePB = rtB(t,L);
			ePA = rtA(t);
			
			Point2D.Double ePD = new Point2D.Double(
					ePA.getX()-k*(ePB.getY()-ePA.getY()),
					ePA.getY()+k*(ePB.getX()-ePA.getX())
					);
			Point2D.Double ePC = new Point2D.Double(
					ePB.getX()-k*(ePB.getY()-ePA.getY()),
					ePB.getY()+k*(ePB.getX()-ePA.getX())
					);
			g2D.drawLine((int)((sPD.x+sPC.x)/2), (int)(sPD.y+sPC.y)/2, (int)(ePD.x+ePC.x)/2, (int)(ePD.y+ePC.y)/2);
			//g2D.drawLine((int)sPD.x, (int)sPD.y, (int)ePD.x, (int)ePD.y);
			//g2D.drawLine((int)sPC.x, (int)sPC.y, (int)ePC.x, (int)ePC.y);
		}
	}
	
	public void drawLines(Graphics2D g2D ,double L, double delta) {
		Point2D.Double sP = rtB(0,L);
		Point2D.Double eP = null;
		double t;
		for(t = delta; t<= 1; t+=delta)
		{
			eP = rtB(t,L);
			g2D.drawLine((int)sP.x, (int)sP.y, (int)eP.x, (int)eP.y);
			sP = eP;
		}
		if(t!=1){
			eP = rtB(1,L);
			g2D.drawLine((int)sP.x, (int)sP.y, (int)eP.x, (int)eP.y);			
		}
		
	}	
	
	public Point2D.Double rtB(double t, double L) {
		double x = ((A.x * t + B.x) * t + C.x) * t + D.x;
		double y = ((A.y * t + B.y) * t + C.y) * t + D.y;

		double dx = (3 * A.x * t + 2 * B.x) * t + C.x;
		double dy = (3 * A.y * t + 2 * B.y) * t + C.y;

		double dNorm = norm(dx, dy);
		double temp = dx;
		dx = L * dy / dNorm;
		dy = -L * temp / dNorm;

		return new Point2D.Double(x + dx, y + dy);
	}

	public double norm(double x, double y) {
		return Math.sqrt(x * x + y * y);
	}
}
