package trajectory;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.List;
import java.awt.MediaTracker;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.swing.JPanel;
import javax.swing.event.MouseInputAdapter;

public class JPanelDisplay extends JPanel {
	
	static final double L = 40;
	static final double M = 40;

	private Image img;
	public double xScale;
	public double yScale;
	public Point2D.Double scalePoint;
	public ArrayList<Controls> controls;
	public Controls selected;
	public int selectedIdx = -1;// left=1, center=0, right=2
	public ArrayList<Point2D> A, B;
	public ArrayList<BezierCurve> C;
	

	private int imgHeight, imgWidth;

	public boolean isAorB;// A=true, B=false;

	private static final long serialVersionUID = 3023667936664038923L;

	public JPanelDisplay(Image img) {
		super();
		init(img);
	}

	public JPanelDisplay(String fName) {
		super();
		init(lodImage(fName));
	}

	private void init(Image img) {	
		A = new ArrayList<>();
		B = new ArrayList<>();
		C = new ArrayList<>();
		this.setBackground(Color.white);
		this.img = img;
		imgHeight = img.getHeight(null);
		imgWidth = img.getWidth(null);
		//this.setPreferredSize(new Dimension(imgWidth*2, imgHeight*2));
		//this.scrollRectToVisible(new Rectangle(0, 0, imgWidth*2, imgHeight*2));

		MouseHandler handler = new MouseHandler();
		this.addMouseListener(handler);
		this.addMouseMotionListener(handler);

		controls = new ArrayList<Controls>();
		xScale = 1;
		yScale = 1;

		isAorB = true;
	}

	private Image lodImage(String fName) {
		MediaTracker tracker = new MediaTracker(this);
		Image openedImage = getToolkit().getImage(fName);
		tracker.addImage(openedImage, 1);
		try {
			tracker.waitForAll();
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}
		return openedImage;
	}

	public Point2D.Double convert1(Point2D.Double p) {
		Point2D.Double temp = new Point2D.Double(p.x / xScale,
				imgHeight - p.y / yScale);
		return temp;
	}

	public Point2D.Double convert2(Point2D.Double p) {
		Point2D.Double temp = new Point2D.Double(p.x * xScale,
				(imgHeight - p.y) * yScale);
		return temp;
	}

	public void paint(Graphics g) {
//		for(int i=0;i<controls.size();i++) {
//			System.out.print(controls.get(i).ctrlCenter.center.getX() + " ");
//			System.out.print(controls.get(i).ctrlLeft.center.getX() + " ");
//			System.out.print(controls.get(i).ctrlRight.center.getX() + " ");
//			System.out.println(controls.get(i).isAorB);
//		}
		System.out.println(A.size());
		
		Color edgeColor = Color.BLUE;
		Color lineColor = Color.LIGHT_GRAY;
		Color selectedColor = Color.RED;

		g.drawImage(img, 0, 0, null);
		
		//g.drawImage(img.getScaledInstance(-1,-1,Image. SCALE_SMOOTH), 0, 0, this);
		
		Graphics2D g2D = (Graphics2D) g;

		g2D.setStroke(new BasicStroke(2));
		g2D.setColor(Color.orange);
		Point2D pre = null;
		
		for (Point2D point2d : A) {
			if(pre != null) {
				//System.out.println("AA");
				if(pre != point2d)
				g2D.drawLine((int)point2d.getX(), (int)point2d.getY(), (int)pre.getX(), (int)pre.getY());
			}
			pre = point2d;
		}
		g2D.setColor(Color.blue);
		pre = null;
		
		for (Point2D point2d : B) {
			if(pre != null) {
				//System.out.println("AA");
				if(pre != point2d)
				g2D.drawLine((int)point2d.getX(), (int)point2d.getY(), (int)pre.getX(), (int)pre.getY());
			}
			pre = point2d;
		}
		
		if(C.size()>0) {
		for (BezierCurve bc : C) {
			g2D.setColor(Color.RED);
			bc.draw(g2D, L);
		}
		}
		
		
		int size = controls.size();
		if (size == 0)
			return;

		// Point2D.Double sPoint = null;
		Controls sControls = null, eControls = null;

		sControls = controls.get(0);
		// sPoint = sControls.ctrlCenter.getCenter();
		g2D.setColor(selectedColor);
		g2D.drawString(Double.toString(sControls.velocity),
				(float) sControls.ctrlCenter.getCenter().getX(),
				(float) sControls.ctrlCenter.getCenter().getY() + 20);

		if (size == 1) {
			g2D.setColor(selectedColor);
			sControls.ctrlCenter.draw(g2D);
			sControls.ctrlLeft.draw(g2D);
			sControls.ctrlRight.draw(g2D);
			
			//System.out.println(sControls.ctrlLeft.center.x);
			g2D.setColor(lineColor);
			Line2D.Double lineLeft = new Line2D.Double(
					sControls.ctrlLeft.getCenter(),
					sControls.ctrlCenter.getCenter());
			Line2D.Double lineRight = new Line2D.Double(
					sControls.ctrlCenter.getCenter(),
					sControls.ctrlRight.getCenter());
			g2D.draw(lineLeft);
			g2D.draw(lineRight);
		}

		for (int i = 1; i < size; i++) {

			eControls = controls.get(i);
			if (eControls.isAorB != sControls.isAorB) {
				sControls = eControls;
				continue;
			}
 
			Point2D.Double cP0 = sControls.ctrlCenter.getCenter();
			Point2D.Double cP1 = sControls.ctrlLeft.getCenter();
			Point2D.Double cP2 = eControls.ctrlRight.getCenter();
			Point2D.Double cP3 = eControls.ctrlCenter.getCenter();

			g2D.setColor(selectedColor);
			g2D.drawString(Double.toString(eControls.velocity),
					(float) cP3.getX(), (float) cP3.getY() + 20);

			BezierCurve cubicDouble = new BezierCurve(cP0, cP1, cP2, cP3);

			g2D.setColor(edgeColor);
			double l;
			if (eControls.isAorB)
				l = L;
			else
				l = -L;
			// sPoint =
			cubicDouble.draw(g2D, l);

			if (eControls.visibility || sControls.visibility) {
				g2D.setColor(selectedColor);
				eControls.ctrlRight.draw(g2D);
				sControls.ctrlLeft.draw(g2D);
				if (eControls.visibility)
					eControls.ctrlCenter.draw(g2D);
				else
					sControls.ctrlCenter.draw(g2D);

				g2D.setColor(lineColor);

				Line2D.Double lineLeft = new Line2D.Double(cP0, cP1);

				Line2D.Double lineRight = new Line2D.Double(cP3, cP2);

				g2D.draw(lineRight);
				g2D.draw(lineLeft);
			}
			sControls = eControls;
		}
	}

	public class MouseHandler extends MouseInputAdapter {

		Point2D.Double locationChange(Point2D.Double p0, Point2D.Double p1) {
			double x = 2 * p0.x - p1.x;
			double y = 2 * p0.y - p1.y;
			return new Point2D.Double(x, y);
		}

		public void mousePressed(MouseEvent e) {
			int size = controls.size();
			Point2D.Double clickPoint = new Point2D.Double(e.getX(), e.getY());
			int idx = 0;
			int selectedIdx;
			for (; idx < size; idx++) {
				Controls item = controls.get(idx);
				selectedIdx = item.contains(clickPoint);
				if (selectedIdx > -1) {
					if (selected != null)
						selected.visibility = false;
					JPanelDisplay.this.selectedIdx = selectedIdx;
					selected = item;
					selected.visibility = true;
					break;
				}
			}
			if (idx == size) {
				JPanelDisplay.this.selectedIdx = -1;
				if (selected != null) {
					selected.visibility = false;
				}
				selected = new Controls(
						new Point2D.Double(clickPoint.getX(), clickPoint.getY()),
						new Point2D.Double(clickPoint.getX(), clickPoint.getY()),
						new Point2D.Double(clickPoint.getX(), clickPoint.getY()),
						isAorB);
				selected.visibility = true;
				if (controls.size() > 0) {
					Controls lastItem = controls.get(controls.size() - 1);

					if (lastItem.isAorB != selected.isAorB) {
						double l;
						if (selected.isAorB)
							l = L;
						else
							l = -L;
						controls.add(lastItem.getMove(l));
					}
				}
				controls.add(selected);
			}
			JPanelDisplay.this.repaint();
		}

		public void mouseDragged(MouseEvent e) {
			Point2D.Double clickPoint = new Point2D.Double(e.getX(), e.getY());

			switch (selectedIdx) {
			case -1:
				if (controls.size() == 0)
					break;
				selected.setRight(clickPoint);
				double x = 2 * selected.ctrlCenter.center.getX()
						- clickPoint.getX();
				double y = 2 * selected.ctrlCenter.center.getY()
						- clickPoint.getY();
				selected.setLeft(new Point2D.Double(x, y));
				break;
			case 0:

				int idx = controls.indexOf(selected);
				double dx = clickPoint.x - selected.ctrlCenter.center.x;
				double dy = clickPoint.y - selected.ctrlCenter.center.y;
				if (idx > 0 && controls.get(idx - 1).isAorB != selected.isAorB)
					controls.get(idx - 1).setCenterMoveDelta(dx, dy);
				else if (idx + 1 < controls.size()
						&& controls.get(idx + 1).isAorB != selected.isAorB)
					controls.get(idx + 1).setCenterMoveDelta(dx, dy);

				selected.setCenterMove(clickPoint);
				break;
			case 1:
				idx = controls.indexOf(selected);
				double l;
				if (selected.isAorB)
					l = -L;
				else
					l = L;
				Controls temp = selected.getMove(l);
				if (idx > 0 && controls.get(idx - 1).isAorB != selected.isAorB)
					controls.set(idx - 1, temp);
				else if (idx + 1 < controls.size()
						&& controls.get(idx + 1).isAorB != selected.isAorB) {
					controls.set(idx + 1, temp);
					System.out.println("Yes");
				}

				selected.setLeft(clickPoint);
				selected.setRight(locationChange(
						selected.ctrlCenter.getCenter(), clickPoint));
				break;
			case 2:
				idx = controls.indexOf(selected);				
				if (selected.isAorB)
					l = -L;
				else
					l = L;
				temp = selected.getMove(l);
				if (idx > 0 && controls.get(idx - 1).isAorB != selected.isAorB)
					controls.set(idx - 1, temp);
				else if (idx + 1 < controls.size()
						&& controls.get(idx + 1).isAorB != selected.isAorB) {
					controls.set(idx + 1, temp);
					System.out.println("Yes");
				}
				
				selected.setRight(clickPoint);
				selected.setLeft(locationChange(
						selected.ctrlCenter.getCenter(), clickPoint));
				break;
			default:
				break;
			}
			JPanelDisplay.this.repaint();
		}
	}
}