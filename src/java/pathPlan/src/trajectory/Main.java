package trajectory;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.ActionMapUIResource;

public class Main extends JFrame implements Runnable {

	private static int PreX=-1, PreY;
	   private static int PreBX, PreBY;
	   private static int NowX, NowY;	
	   Thread t1;
	private static final long serialVersionUID = -9094128466207013760L;
	public static String dirpath=".";
	JPanelDisplay display = null;
	JTextField txtFieldVelocity;

	public static void main(String[] args) {
		Main app = new Main();
		
		app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		app.setResizable(true);
		app.setVisible(true);
	}

	public void setTxtVelocity(String txt) {
		txtFieldVelocity.setText(txt);
	}
	

	public Main() {
		
		t1 = new Thread(this);
		t1.start();
		this.setLayout(new BorderLayout());
		
		

		JMenuBar menubar = new JMenuBar();

		JMenu file = new JMenu("File");
		file.setMnemonic(KeyEvent.VK_F);

		JMenu help = new JMenu("Тохиргоо");
		help.setMnemonic(KeyEvent.VK_R);

		JMenuItem about = new JMenuItem("About");
		help.add(about);

		menubar.add(file);
		menubar.add(help);
		setJMenuBar(menubar);

		String fName = "img/data.jpg";
		display = new JPanelDisplay(fName);
		
		JScrollPane scrollPane = new JScrollPane(display);
		//ImageZoom zoom = new ImageZoom(display);
		this.add(scrollPane, BorderLayout.CENTER);
		//this.getContentPane().add(zoom.getUIPanel(), "North");
		//this.getContentPane().add(new JScrollPane(display));

		JPanel panelBoth = new JPanel();
		panelBoth.setPreferredSize(new Dimension(120, 100));
		this.add(panelBoth, BorderLayout.LINE_END);
		panelBoth.setLayout(null);

		TitledBorder title;
		title = BorderFactory.createTitledBorder("Command line");
		panelBoth.setBorder(title);

		JButton btnUndo = new JButton("Буцах");
		btnUndo.setBounds(10, 20, 100, 25);
		btnUndo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (display == null || display.controls == null
						|| display.controls.size() == 0)
					return;
				int size = display.controls.size();
				display.controls.remove(--size);
				if (size >= 2
						&& display.controls.get(size - 1).isAorB != display.controls
								.get(size - 2).isAorB) {
					display.controls.remove(--size);
				}
				display.repaint();
			}
		});
		btnUndo.setMnemonic(KeyEvent.VK_Z);
		panelBoth.add(btnUndo);
		
		
		JButton btnSave = new JButton("Хадгалах");
		btnSave.setBounds(10, 55, 100, 25);
		btnSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (display.controls == null || display.controls.size() <= 1) {
					JOptionPane.showMessageDialog(Main.this,
							"Хадгалах цэг байхгүй байна.", "Анхаар!",
							JOptionPane.WARNING_MESSAGE);
					return;
				}
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setCurrentDirectory(new File(dirpath));
				
				int result = fileChooser.showSaveDialog(display);
				if (result == JFileChooser.APPROVE_OPTION) {	
					dirpath =fileChooser.getSelectedFile().getAbsolutePath().toString();
					FileWriter fw = null;
					BufferedWriter bw = null;
					try {
						fw = new FileWriter(fileChooser.getSelectedFile());
						bw = new BufferedWriter(fw);
						int size = display.controls.size();
						if (size == 0)
							return;
						Controls sControl = display.controls.get(0);

						for (int i = 1; i < size; i++) {
							Controls eControl;
							eControl = display.controls.get(i);
							
							
							if (sControl.isAorB == eControl.isAorB) {

								Point2D.Double cP0 = display
										.convert2(sControl.ctrlCenter
												.getCenter());
								
								Point2D.Double cP1 = display
										.convert2(sControl.ctrlLeft.getCenter());
								Point2D.Double cP2 = display
										.convert2(eControl.ctrlRight
												.getCenter());
								Point2D.Double cP3 = display
										.convert2(eControl.ctrlCenter
												.getCenter());
								//cP0 = sControl.ctrlCenter.getCenter();
								//cP1 = sControl.ctrlLeft.getCenter();
								//cP2 = eControl.ctrlRight.getCenter();
								//cP3 = eControl.ctrlCenter.getCenter();

								try {

									if (sControl.isAorB)
										bw.write("A ");
									else
										bw.write("B ");
									bw.write(String.valueOf(cP0.getX()) + " "
											+ String.valueOf(cP0.getY()) + " ");
									bw.write(String.valueOf(cP1.getX()) + " "
											+ String.valueOf(cP1.getY()) + " ");
									bw.write(String.valueOf(cP2.getX()) + " "
											+ String.valueOf(cP2.getY()) + " ");
									bw.write(String.valueOf(cP3.getX()) + " "
											+ String.valueOf(cP3.getY()) + " ");
									bw.write(String.valueOf(eControl.velocity) + " ");
									bw.write(String.valueOf(eControl.velocity));
									//bw.write(System.lineSeparator());

									// bw.newLine();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}
							sControl = eControl;
						}
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} finally {
						try {
							bw.flush();
							bw.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		});
		btnSave.setMnemonic(KeyEvent.VK_S);
		panelBoth.add(btnSave);
		
		JButton btnOpen = new JButton("Open");
		btnOpen.setBounds(10, 100, 100, 25);
		
		btnOpen.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				JFileChooser fileChooser = new JFileChooser();
				
				fileChooser.setCurrentDirectory(new File(dirpath));
				//
				int result = fileChooser.showOpenDialog(display);
				dirpath =fileChooser.getSelectedFile().getAbsolutePath().toString();
				if (result == JFileChooser.APPROVE_OPTION) {

					FileReader fw = null;
					BufferedReader bw = null;
					try {
						
						fw = new FileReader(fileChooser.getSelectedFile());
						bw = new BufferedReader(fw);
						int size = display.controls.size();
						while(size>0) {
							display.controls.remove(--size);
						}
						
						String line = null;
						String[] parts;
						Controls sControls = new Controls(new Point2D.Double(1,1), new Point2D.Double(1,1),new Point2D.Double(1,1),true);
						
						boolean flag = true;
						double x=0, y=0;
						
						
						while((line = bw.readLine()) != null) {
							
							Controls eControls = new Controls(new Point2D.Double(1,1), new Point2D.Double(1,1),new Point2D.Double(1,1),true);
							parts = line.split(" ");
							boolean  b1;
							if(parts[0].equals(new String("A"))) b1 = true;
							else b1 = false;
							//System.out.println(b1);
							Point2D.Double cP0 = new Point2D.Double(Double.parseDouble(parts[1]), Double.parseDouble(parts[2]));
							Point2D.Double cP1 = new Point2D.Double(Double.parseDouble(parts[3]), Double.parseDouble(parts[4]));
							Point2D.Double cP2 = new Point2D.Double(Double.parseDouble(parts[5]), Double.parseDouble(parts[6]));
							Point2D.Double cP3 = new Point2D.Double(Double.parseDouble(parts[7]), Double.parseDouble(parts[8]));
							cP0 = display.convert1(cP0);
							cP1 = display.convert1(cP1);
							cP2 = display.convert1(cP2);
							cP3 = display.convert1(cP3);
							Point2D.Double temp = new Point2D.Double((2*cP0.getX()-cP1.getX()), 2*cP0.getY()-cP1.getY());
							
							sControls.ctrlCenter.setLocation(cP0.getX(), cP0.getY());
							sControls.ctrlLeft.setLocation(cP1.getX(), cP1.getY());
							sControls.ctrlRight.setLocation(temp);
							
							sControls.velocity = 10;
							if(flag == false) {
								flag = true;
								sControls = new Controls(cP0, cP1, temp, b1);
							}
							sControls.visibility = false;
							sControls.isAorB = b1;
							if(display.controls.size()>0){
								Controls lastItem = display.controls.get(display.controls.size()-1);
								flag = (lastItem.isAorB == sControls.isAorB);
								if(!flag) {
									
									//System.out.println(b1);
									
									double l;
									if (sControls.isAorB)
										l = -display.L;
									else
										l = display.L;
									
									display.controls.add(sControls.getMove(l));
								}
								
							}
							display.controls.add(sControls);
							eControls.ctrlRight.setLocation(cP2);
							eControls.ctrlCenter.setLocation(cP3);
							eControls.isAorB = b1;
							sControls = eControls;
							//break;
						}
						sControls.ctrlLeft.setLocation(2*sControls.ctrlCenter.center.x-sControls.ctrlRight.center.x, 2*sControls.ctrlCenter.center.y-sControls.ctrlRight.center.x);
						if(display.controls.size()>0) {
							Controls lastItem = display.controls.get(display.controls.size()-1);
							if(sControls.isAorB != lastItem.isAorB) {
								double l;
								if (sControls.isAorB)
									l = -display.L;
								else
									l = display.L; 
								display.controls.add(sControls.getMove(l));
							}
						}
						
						display.controls.add(sControls);
						
						display.repaint();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} finally {
						try {
							//bw.flush();
							bw.close();
						} catch (IOException e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}
					}
					
				}
			}
		});
		btnOpen.setMnemonic(KeyEvent.VK_O);
		panelBoth.add(btnOpen);
		
		txtFieldVelocity = new JTextField();
		txtFieldVelocity.setBounds(10, 130, 100, 25);
		txtFieldVelocity.getDocument().addDocumentListener(
				new DocumentListener() {

					@Override
					public void removeUpdate(DocumentEvent arg0) {
						set();
					}

					@Override
					public void insertUpdate(DocumentEvent arg0) {
						set();
					}

					@Override
					public void changedUpdate(DocumentEvent arg0) {
						set();
					}

					private void set() {
						if (display == null || display.selected == null)
							return;
						String txt = txtFieldVelocity.getText().trim();
						if (txt.length() == 0)
							return;
						try {
							double temp = Double.parseDouble(txt);
							display.selected.velocity = temp;
							display.repaint();
						} catch (Exception e) {
							e.printStackTrace();
							JOptionPane.showMessageDialog(Main.this,
									"Алдаа гарлаа.", "Алдаа!",
									JOptionPane.ERROR_MESSAGE);
							txtFieldVelocity.setText("");
						}
					}
				});
		panelBoth.add(txtFieldVelocity);

		JRadioButton aButton = new JRadioButton("A дугуй");
		aButton.setMnemonic(KeyEvent.VK_A);
		aButton.setActionCommand("A дугуй");
		aButton.setSelected(true);
		aButton.setBounds(10, 160, 100, 15);
		aButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				display.isAorB = true;
			}
		});

		JRadioButton bButton = new JRadioButton("B дугуй");
		bButton.setMnemonic(KeyEvent.VK_B);
		bButton.setActionCommand("B дугуй");
		bButton.setBounds(10, 200, 100, 15);
		bButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				display.isAorB = false;
			}
		});
		ButtonGroup group = new ButtonGroup();
		group.add(aButton);
		group.add(bButton);

		panelBoth.add(aButton);
		panelBoth.add(bButton);

		JButton btnRun = new JButton("C,D-г зурах");
		btnRun.setMnemonic(KeyEvent.VK_R);
		btnRun.setBounds(10, 240, 100, 25);
		btnRun.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				double size;
				if (display == null || display.controls == null
						|| (size = display.controls.size()) <= 1)
					return;

				Graphics2D g2D = (Graphics2D) display.getGraphics();
				g2D.setStroke(new BasicStroke(2));
				Controls sControls = null, eControls = null;

				sControls = display.controls.get(0);

				for (int i = 1; i < size; i++) {

					eControls = display.controls.get(i);
					if (eControls.isAorB != sControls.isAorB) {
						sControls = eControls;
						continue;
					}

					Point2D.Double cP0 = sControls.ctrlCenter.getCenter();
					Point2D.Double cP1 = sControls.ctrlLeft.getCenter();
					Point2D.Double cP2 = eControls.ctrlRight.getCenter();
					Point2D.Double cP3 = eControls.ctrlCenter.getCenter();

					g2D.setColor(Color.YELLOW);

					double l;
					if (eControls.isAorB)
						l = display.L;
					else
						l = -display.L;
					// sPoint =
					
					BezierCalc cubicDouble = new BezierCalc(cP0, cP1, cP2, cP3);
					cubicDouble.drawLinesCD(g2D, l, display.M, 0.01);
					sControls = eControls;
				}

			}
		});
		panelBoth.add(btnRun);

		setBounds(0, 0, 1000, 700);
		setResizable(false);
	}

	@Override
	public void run() {
		int t=0;
	    /*while(t<100) {
	    	NowX = t;
	    	NowY = t*2;
	    	A.add(new Point(t, t+t));
	    	System.out.println(A.size());
	    	t++;
	    	//mainFrame.repaint();
	    	
	    }*/
		//mainFrame.getMousePosition();
		
		
		double minX= -1500;
		double maxX= 1500;
		double minY= -1500;
		double maxY= 1500;
		String fromclient = null;
	    String toclient;		
	    int portNum = 5002;
	    
	    ServerSocket Server = null;
		try {
			Server = new ServerSocket (portNum);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	         
	    System.out.println ("TCPServer Waiting for client on port " + portNum);
		while(true) 
	    {
			
			
		Socket connected = null;
		try {
			connected = Server.accept();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println( " THE CLIENT"+" "+
				    connected.getInetAddress() +":"+connected.getPort()+" IS CONNECTED ");  
	   
		BufferedReader inFromClient = null;
		try {
			inFromClient = new BufferedReader(new InputStreamReader (connected.getInputStream()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	                
		while ( true )
		  {
			
			Point point = new Point();
			Point point2 = new Point();
		    try {
		    	
				fromclient = inFromClient.readLine();
				//System.out.println("aaa");
				
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				//System.out.println("aaa");
				e1.printStackTrace();
			}
		    if ( fromclient == null ) {
		    	try {
		    		connected.close();
		    	} catch (IOException e) {
			// 	TODO Auto-generated catch block
		    		e.printStackTrace();
		    	}
		    	break;
		    }
		    else {
		    	System.out.println(fromclient);
		    	String parts[] = fromclient.split(" ");
		    	Double type = Double.parseDouble(parts[0]);
		    	
		    	Point2D.Double A = new Point2D.Double();
		    	A.x = Double.parseDouble(parts[1])/2;
		    	A.y = Double.parseDouble(parts[2])/2;

		    	Point2D.Double B = new Point2D.Double();
		    	B.x = Double.parseDouble(parts[3])/2;
		    	B.y = Double.parseDouble(parts[4])/2;
		    	if(type==2) {
		    		Point2D.Double C = new Point2D.Double();
			    	C.x = Double.parseDouble(parts[5])/2;
			    	C.y = Double.parseDouble(parts[6])/2;
			    	Point2D.Double D = new Point2D.Double();
			    	D.x = Double.parseDouble(parts[7])/2;
			    	D.y = Double.parseDouble(parts[8])/2;
			    	System.out.println(parts[8]);
			    	display.C.add(new BezierCurve(A, B,C,D));
		    	}
		    	else {
			    	point2.setLocation(display.getWidth()*(int)(B.x-minX)/(int)(maxX-minX), display.getHeight()*(int)(B.y-minY)/(int)(maxY-minY));
			    	point.setLocation(display.getWidth()*(int)(A.x-minX)/(int)(maxX-minX), display.getHeight()*(int)(A.y-minY)/(int)(maxY-minY));
			    	//point2.setLocation(Bx, By);
			    	//point.setLocation(Ax, Ay);
			    	//System.out.println(point.x+ " " + point.y + " " + point2.x + " " + point2.y);
			    	
			    	//A.add(point);
			    	//B.add(point2);
			    	if(display.A.size()>0) {
			    		if(display.A.get(display.A.size()-1)!=point) display.A.add(point); 
			    	}
			    	else display.A.add(point);
			    	
			    	if(display.B.size()>0) {
			    		if(display.B.get(display.B.size()-1)!=point2) display.B.add(point2); 
			    	}
			    	else display.B.add(point2);
					System.out.println(point.x+" " +point.y);
		    	}
		    	display.repaint();
				t++;
		    }     	
		    }
	    }

		
	}
		

	
	
}




