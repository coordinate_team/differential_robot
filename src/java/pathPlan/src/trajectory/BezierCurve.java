package trajectory;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.Point2D;

public class BezierCurve {

	Marker p0, p1, p2, p3;
	CubicCurve2D cubicCurver;	
	
	public BezierCurve(Point2D.Double p0, Point2D.Double p1, Point2D.Double p2,
			Point2D.Double p3) {
		this.p0 = new Marker(p0);
		this.p1 = new Marker(p1);
		this.p2 = new Marker(p2);
		this.p3 = new Marker(p3);
		
		cubicCurver = new CubicCurve2D.Double(p0.x, p0.y, p1.x, p1.y, p2.x,
				p2.y, p3.x, p3.y);
	}	

	public Point2D.Double draw(Graphics2D g2D, double L) {
		g2D.draw(cubicCurver);
		p0.draw(g2D);
		p3.draw(g2D);		
		BezierCalc calc = new BezierCalc(p0.center, p1.center, p2.center, p3.center);
		g2D.setColor(Color.MAGENTA);		
		calc.drawLines(g2D, L, 0.01);
		g2D.setColor(Color.BLACK);
		calc.drawLines(g2D, L/2, 0.01);
		return calc.rtB(1, L);
	}
}
