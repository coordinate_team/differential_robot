package trajectory;

import java.awt.geom.Point2D;

public class Controls {
	public Marker ctrlCenter, ctrlLeft, ctrlRight;
	public boolean visibility;
	public double velocity;
	public boolean isAorB;

	public Controls getMove(double L) {
		Point2D.Double p0 = ctrlCenter.getCenter();
		Point2D.Double p1 = ctrlLeft.getCenter();
		//System.out.println("getMove:" + isAorB);
		Point2D.Double temp = new Point2D.Double(p1.y - p0.y, p1.x - p0.x);

		double norm = temp.distance(0, 0);

		Point2D.Double pC = new Point2D.Double(p0.x - L * temp.x / norm, p0.y
				+ L * temp.y / norm);
		Point2D.Double pL = new Point2D.Double(p1.x + pC.x - p0.x, p1.y + pC.y
				- p0.y);
		Point2D.Double pR = new Point2D.Double(2 * pC.x - pL.x, 2 * pC.y - pL.y);

		return new Controls(pC, pL, pR, !isAorB);
	}

	public void setCenterMove(Point2D.Double p) {
		double dx = p.x - ctrlCenter.center.x;
		double dy = p.y - ctrlCenter.center.y;

		ctrlLeft.setLocation(ctrlLeft.center.x + dx, ctrlLeft.center.y + dy);
		ctrlRight.setLocation(ctrlRight.center.x + dx, ctrlRight.center.y + dy);
		ctrlCenter.setLocation(p.x, p.y);
	}
	public void setCenterMoveDelta(double dx, double dy) {
		ctrlLeft.setLocation(ctrlLeft.center.x + dx, ctrlLeft.center.y + dy);
		ctrlRight.setLocation(ctrlRight.center.x + dx, ctrlRight.center.y + dy);
		ctrlCenter.setLocation(ctrlCenter.center.x + dx, ctrlCenter.center.y + dy);
	}

	public Controls(boolean isAorB) {
		visibility = false;
		velocity = 10;
		this.isAorB = isAorB;
	}

	public Controls(Point2D.Double pCenter, Point2D.Double pLeft,
			Point2D.Double pRight, boolean isAorB) {
		visibility = false;
		velocity = 10;
		this.isAorB = isAorB;

		setCenter(pCenter);
		setLeft(pLeft);
		setRight(pRight);
	}

	public int contains(Point2D.Double p) {
		if (ctrlCenter.contains(p))
			return 0;
		if (ctrlLeft.contains(p))
			return 1;
		if (ctrlRight.contains(p))
			return 2;
		return -1;
	}

	public void setCenter(Point2D.Double p) {
		if (ctrlCenter == null) {
			ctrlCenter = new Marker(p);
		} else {
			ctrlCenter.setLocation(p);
		}
	}

	public void setLeft(Point2D.Double p) {
		if (ctrlLeft == null) {
			ctrlLeft = new Marker(p);
		} else {
			ctrlLeft.setLocation(p);
		}
	}

	public void setRight(Point2D.Double p) {
		if (ctrlRight == null) {
			ctrlRight = new Marker(p);
		} else {
			ctrlRight.setLocation(p);
		}
	}
}
