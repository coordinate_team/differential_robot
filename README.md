# project README file
---------------------

Repository нь үндсэн *хоёр* хавтастай байна.

* doc/
* src/

## Дүрмүүд

* **doc/** дотор төслийн дэлгэрэнгүй documentation байна. (Жишээ нь: LaTeX файл)

* **src/** дотор төслийн хэрэгжүүлэлт бүхий код байна. Өөр хэл дээр бичигдсэн кодууд өөр өөр хавтаст байрлана. Жишээ нь:
```
src/
├── README.md
├── c/
│   ├── README.md
│   ├── example_code.c
│
├── cpp/
│   ├── README.md
│   ├── example_code.cpp
|
├── matlab/
    ├── README.md
    ├── example_code.m
```

* Хавтас тус бүр дотроо README файлыг агуулсан байна.

* README файлд нэмэлт видео файлын холбоос, кодуудын файл тус бүр ямар үүрэгтэй талаарх нэмэлт мэдээллүүдийг оруулж өгнө.

* README файлыг markdown хэлбэрээр бичнэ. Markdown-ны тухай холбоосыг [энд](https://guides.github.com/features/mastering-markdown/) орууллаа.